package todolist

class BootStrap {

    def items = [
      "Create GIT VM",
      "Create Jenkins VM",
      "Create Web Server VM",
      "Create App Server VM",
      "Create DB Server VM",
      "Create TODO List App",
      "Create Presentation Slides"
    ]

    def init = { servletContext ->

      def lists = TodoList.findAllByName("Presentation")

      if(lists.size<=0){
        def exampleList = new TodoList(name:"Presentation")

        items.each{ curritem ->
            exampleList.addToItems(new ListItem(name:curritem))
        }
        exampleList.save()
      }

    }

    def destroy = {
    }
}
