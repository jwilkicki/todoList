package todolist

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ListItemController {

    ListItemService listItemService
    TodoListService todoListService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond listItemService.list(params), model:[listItemCount: listItemService.count()]
    }

    def show(Long id) {

        ListItem createdItem = listItemService.get(id)
        if(createdItem==null){
          response.sendError(404)
        }else{
          redirect(controller: 'todoList',action: "show",params: [id: createdItem.list.id])
        }
    }

    def create() {
        System.out.println("Params ${params}")
        def model = ['listItem': new ListItem(params),'parent': params['list_id']]
        respond model
    }

    def save(ListItem listItem) {
        if (listItem == null) {
            notFound()
            return
        }

        try {
            listItemService.save(listItem)
        } catch (ValidationException e) {
            respond listItem.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'listItem.label', default: 'ListItem'), listItem.id])
                redirect listItem
            }
            '*' { respond listItem, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond listItemService.get(id)
    }

    def update(ListItem listItem) {
        if (listItem == null) {
            notFound()
            return
        }

        try {
            listItemService.save(listItem)
        } catch (ValidationException e) {
            respond listItem.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'listItem.label', default: 'ListItem'), listItem.id])
                redirect listItem
            }
            '*'{ respond listItem, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        ListItem toDelete = listItemService.get(id)
        println "To Delete: ${toDelete.name} List: ${toDelete.list.id}"
        listItemService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'listItem.label', default: 'ListItem'), id])
                redirect(controller: "todoList", action:"show", method:"GET", id: toDelete.list.id)
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'listItem.label', default: 'ListItem'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
