package todolist

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ListItemServiceSpec extends Specification {

    TodoListService todoListService
    ListItemService listItemService
    SessionFactory sessionFactory

    private Long setupData() {
        TodoList testList = new TodoList(name: "My TODO List").save()

        ListItem itemOne = new ListItem(name: "Item One",list: testList).save()

        4.times {
          new ListItem(name: "Item ${it}",list: testList).save()
        }

        itemOne.id
    }

    void "test get"() {
        setupData()

        expect:
        listItemService.get(2) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ListItem> listItemList = listItemService.list(max: 2, offset: 2)

        then:
        listItemList.size() == 2
        assert listItemList[0].id == 3
        assert listItemList[1].id == 4
    }

    void "test count"() {
        setupData()

        expect:
        listItemService.count() == 12
    }

    void "test delete"() {
        Long listItemId = setupData()

        expect:
        listItemService.count() == 12

        when:
        listItemService.delete(listItemId)
        sessionFactory.currentSession.flush()

        then:
        listItemService.count() == 11
    }

    void "test save"() {
        when:

        ListItem listItem = new ListItem(name: "Test Item Save",list: todoListService.get(1))
        listItemService.save(listItem)

        then:
        listItem.id != null
    }
}
