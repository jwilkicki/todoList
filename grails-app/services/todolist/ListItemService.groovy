package todolist

import grails.gorm.services.Service

@Service(ListItem)
interface ListItemService {

    ListItem get(Serializable id)

    List<ListItem> list(Map args)

    Long count()

    void delete(Serializable id)

    ListItem save(ListItem listItem)

}