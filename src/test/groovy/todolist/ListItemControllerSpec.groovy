package todolist

import grails.testing.gorm.DomainUnitTest
import grails.testing.gorm.DataTest
import grails.testing.web.controllers.ControllerUnitTest
import grails.validation.ValidationException
import spock.lang.*

class ListItemControllerSpec extends Specification implements ControllerUnitTest<ListItemController>, DomainUnitTest<ListItem>, DataTest {

  void setupSpec(){
    mockDomain TodoList
  }

    def populateValidParams(params) {
        assert params != null

        params["name"] = "Test Item"
        params["list"] = new TodoList(name:"Test List",id: 1)
    }

    void "Test the index action returns the correct model"() {
        given:
        controller.listItemService = Mock(ListItemService) {
            1 * list(_) >> []
            1 * count() >> 0
        }

        when:"The index action is executed"
        controller.index()

        then:"The model is correct"
        !model.listItemList
        model.listItemCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
        controller.create()

        then:"The model is correctly created"
        model.listItem!= null
    }

    void "Test the save action with a null instance"() {
        when:"Save is called for a domain instance that doesn't exist"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'POST'
        controller.save(null)

        then:"A 404 error is returned"
        response.redirectedUrl == '/listItem/index'
        flash.message != null
    }

    void "Test the save action correctly persists"() {
        given:
        controller.listItemService = Mock(ListItemService) {
            1 * save(_ as ListItem)
        }

        when:"The save action is executed with a valid instance"
        response.reset()
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'POST'
        populateValidParams(params)
        def listItem = new ListItem(params)
        listItem.id = 1

        controller.save(listItem)

        then:"A redirect is issued to the show action"
        response.redirectedUrl == '/listItem/show/1'
        controller.flash.message != null
    }

    void "Test the save action with an invalid instance"() {
        given:
        controller.listItemService = Mock(ListItemService) {
            1 * save(_ as ListItem) >> { ListItem listItem ->
                throw new ValidationException("Invalid instance", listItem.errors)
            }
        }

        when:"The save action is executed with an invalid instance"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'POST'
        def listItem = new ListItem()
        controller.save(listItem)

        then:"The create view is rendered again with the correct model"
        model.listItem != null
        view == 'create'
    }

    void "Test the show action with a null id"() {
        given:
        controller.listItemService = Mock(ListItemService) {
            1 * get(null) >> null
        }

        when:"The show action is executed with a null domain"
        controller.show(null)

        then:"A 404 error is returned"
        response.status == 404
    }

    void "Test the edit action with a null id"() {
        given:
        controller.listItemService = Mock(ListItemService) {
            1 * get(null) >> null
        }

        when:"The show action is executed with a null domain"
        controller.edit(null)

        then:"A 404 error is returned"
        response.status == 404
    }

    void "Test the edit action with a valid id"() {
        given:
        controller.listItemService = Mock(ListItemService) {
            1 * get(2) >> new ListItem()
        }

        when:"A domain instance is passed to the show action"
        controller.edit(2)

        then:"A model is populated containing the domain instance"
        model.listItem instanceof ListItem
    }


    void "Test the update action with a null instance"() {
        when:"Save is called for a domain instance that doesn't exist"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'PUT'
        controller.update(null)

        then:"A 404 error is returned"
        response.redirectedUrl == '/listItem/index'
        flash.message != null
    }

    void "Test the update action correctly persists"() {
        given:
        controller.listItemService = Mock(ListItemService) {
            1 * save(_ as ListItem)
        }

        when:"The save action is executed with a valid instance"
        response.reset()
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'PUT'
        populateValidParams(params)
        def listItem = new ListItem(params)
        listItem.id = 1

        controller.update(listItem)

        then:"A redirect is issued to the show action"
        response.redirectedUrl == '/listItem/show/1'
        controller.flash.message != null
    }

    void "Test the update action with an invalid instance"() {
        given:
        controller.listItemService = Mock(ListItemService) {
            1 * save(_ as ListItem) >> { ListItem listItem ->
                throw new ValidationException("Invalid instance", listItem.errors)
            }
        }

        when:"The save action is executed with an invalid instance"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'PUT'
        controller.update(new ListItem())

        then:"The edit view is rendered again with the correct model"
        model.listItem != null
        view == 'edit'
    }

    void "Test the delete action with a null instance"() {
        when:"The delete action is called for a null instance"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'DELETE'
        controller.delete(null)

        then:"A 404 is returned"
        response.redirectedUrl == '/listItem/index'
        flash.message != null
    }

    void "Test the delete action with an instance"() {
        given:
        TodoList testList = new TodoList(name: "Test List").save()
        controller.listItemService = Mock(ListItemService) {
            1 * get(2) >> {
              ListItem toReturn = new ListItem(name: "Test Item", list: testList)
            }
            1 * delete(2)
        }

        when:"The domain instance is passed to the delete action"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'DELETE'
        controller.delete(2)

        then:"The user is redirected to index"
        response.redirectedUrl == '/todoList/show/1'
        flash.message != null
    }
}
