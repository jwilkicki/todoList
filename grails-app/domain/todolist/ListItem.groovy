package todolist

class ListItem {

    String name
    static belongsTo = [list: TodoList]

    static constraints = {
    }
}
