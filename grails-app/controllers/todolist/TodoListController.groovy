package todolist

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class TodoListController {

    TodoListService todoListService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond todoListService.list(params), model:[todoListCount: todoListService.count()]
    }

    def show(Long id) {
        respond todoListService.get(id)
    }

    def create() {
        respond new TodoList(params)
    }

    def save(TodoList todoList) {
        if (todoList == null) {
            notFound()
            return
        }

        try {
            todoListService.save(todoList)
        } catch (ValidationException e) {
            respond todoList.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'todoList.label', default: 'TodoList'), todoList.name])
                redirect todoList
            }
            '*' { respond todoList, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond todoListService.get(id)
    }

    def update(TodoList todoList) {
        if (todoList == null) {
            notFound()
            return
        }

        try {
            todoListService.save(todoList)
        } catch (ValidationException e) {
            respond todoList.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'todoList.label', default: 'TodoList'), todoList.id])
                redirect todoList
            }
            '*'{ respond todoList, [status: OK] }
        }
    }

    def delete(Long id,String name) {
        if (id == null) {
            notFound()
            return
        }

        todoListService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'todoList.label', default: 'TodoList'), name])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'todoList.label', default: 'TodoList'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
