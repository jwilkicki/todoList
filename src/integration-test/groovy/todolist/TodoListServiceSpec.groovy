package todolist

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class TodoListServiceSpec extends Specification {

    TodoListService todoListService
    SessionFactory sessionFactory

    private Long setupData() {

        TodoList todoList = new TodoList(name: "Int Test List").save(flush: true, failOnError: true)

        3.times {
          new TodoList(name: "TODO List ${it}").save(flush: true,failOnError: true)
        }

        todoList.id
    }

    void "test get"() {
        setupData()

        expect:
        todoListService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<TodoList> todoListList = todoListService.list(max: 2, offset: 2)

        then:
        todoListList.size() == 2
        assert todoListList[0].id == 11
        assert todoListList[1].id == 12
    }

    void "test count"() {
        setupData()

        expect:
        todoListService.count() == 5
    }

    void "test delete"() {
        Long todoListId = setupData()

        expect:
        todoListService.count() == 5

        when:
        todoListService.delete(todoListId)
        sessionFactory.currentSession.flush()

        then:
        todoListService.count() == 4
    }

    void "test save"() {
        when:

        TodoList todoList = new TodoList(name: "Save New List")
        todoListService.save(todoList)

        then:
        todoList.id != null
    }
}
