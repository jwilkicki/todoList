package todolist

import grails.gorm.services.Service

@Service(TodoList)
interface TodoListService {

    TodoList get(Serializable id)

    List<TodoList> list(Map args)

    Long count()

    void delete(Serializable id)

    TodoList save(TodoList todoList)

}