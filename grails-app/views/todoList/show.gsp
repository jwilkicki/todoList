<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'todoList.label', default: 'TODO List')}" />
        <g:set var="domainProperties" value="['name']" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-todoList" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="show-todoList" class="content scaffold-show" role="main">
            <h1><f:display bean="todoList" property="name"/></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>

            <table>
              <thead>
                   <tr>
                      <g:each in="${domainProperties}" var="p" status="i">
                          <g:sortableColumn property="${p}" title="${p.capitalize()}" />
                      </g:each>
                  </tr>
              </thead>
              <tbody>
                  <g:each in="${todoList.items}" var="bean" status="i">
                      <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                          <g:each in="${domainProperties}" var="p" status="j">

                              <td><f:display bean="${bean}" property="${p}"  displayStyle="${displayStyle?:'table'}" theme="${theme}"/></td>

                          </g:each>
                          <td>
                            <g:form resource="${bean}" params="[id:bean.id]" method="DELETE">
                              <g:link class="edit" controller="listItem" action="edit" id="${bean.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link> | <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                            </g:form>
                          </td>
                      </tr>
                  </g:each>
                  <tr>
                      <td><g:link controller="listItem" action="create" params="[list_id: todoList.id,list_name: todoList.name]">Add List Item</g:link></td>
                  </tr>
              </tbody>
            </table>

            <g:form resource="${this.todoList}" params="[id:this.todoList.id,name:this.todoList.name]" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit" action="edit" resource="${this.todoList}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
